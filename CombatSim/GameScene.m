//
//  GameScene.m
//  CombatSim
//
//  Created by Vaster on 3/25/17.
//  Copyright © 2017 Vaster. All rights reserved.
//

#import "GameScene.h"

//Masks of the two coliding objects
static const uint32_t mageFireCategory = 0x1 << 0;
static const uint32_t impCategory =  0x1 << 1;
static const uint32_t playerCategory =  0x1 << 2;
static const uint32_t groundCategory =  0x1 << 3;
static const uint32_t fireBallCategory = 0x1 << 4;
static const uint32_t backGroundCategory = 0x1 << 5;
static const uint32_t platformCategory = 0x1 << 6;




//static const uint32_t backgroundCategory =  0x1 << 3;

@implementation GameScene {
    NSTimeInterval _lastUpdateTime;
    
    SKSpriteNode * background;
    SKSpriteNode * ground;
    SKSpriteNode * player;
    SKSpriteNode * fireButton;
    SKSpriteNode * fireBall;
    SKSpriteNode * resetButton;
    SKSpriteNode * platform[11];
    SKSpriteNode * mageFireBall[2];


    
    //Can have up to 6 imps
    SKSpriteNode * imp[6];
    Boolean impDirect[6];
    
    //Mage imp
    SKSpriteNode * mageImp[2];
    Boolean mageDirect[2];
    Boolean mageAlive[2];
    
    //Bird enenmy
    SKSpriteNode * bird [2];
    Boolean birdDirect[2];
    
    //Player actions
    SKAction* jumpLeft;
    SKAction* walkLeft;
    SKAction* jumpRight;
    SKAction* walkRight;
    SKAction* death;
    SKAction* hit;
    
    SKTexture* celesHit;
    SKTexture* celesRight;
    SKTexture* celesLeft;
    SKTexture* celesIdle;
    
    //Imp Actions
    SKAction * impRight;
    SKAction * impLeft;
    
    //Bird Actions
    SKAction * birdFly;
    SKTexture * birdDead;
    SKAction * moveRightSide;
    SKAction * moveLeftSide;
    
    //Int player hp
    int playerHealth;

    //True is player left false is player is right
    Boolean playerLeft;
    
    SKCameraNode * cam;
    
    int rightBounds;
    int leftBounds;
    
    //Timeframe counter
    int frameCounter;
    int impScore;

}

- (void)sceneDidLoad {
    static BOOL didLoad = NO;
    
    
    //Scene is being loaded twice for some reason, camera only works on second load
    //LOOKS INTO THIS AT SOME POINT BUT THIS IS A TEMP FIX
    if (didLoad)
    {
        cam = [SKCameraNode node];
        self.camera = cam;
        //NSLog(@"Skipping the second load");
        return;
    }
    didLoad = YES;
    
    impScore = 0;
   
    printf("Called twiced \n");
    
    //Make background
    
    [self makeBackground:0];
    [self makeBackground:background.size.width - 230];
    [self makeBackground:0-background.size.width + 384];
    
    //Set World gravity
    self.physicsWorld.gravity = CGVectorMake( 0.0, -2.6 );
    self.physicsWorld.contactDelegate = self;
    
    
    //Get bounds of level
    rightBounds = 800;
    leftBounds = -800;

    
    //Make ground
    [self makeGround];

    //Make player
    [self makePlayer];
    [self makeAnimation];
    [self makeFireButton];
    [self makeResetButton];
    [self makeImps];
    [self makeMageImps];
    [self makePlatform];
    [self makeBirds];
 }

-(void) makeBackground:(int)xLoc{
    background = [SKSpriteNode spriteNodeWithImageNamed: @"background.png"];
    background.size = CGSizeMake(background.size.width ,self.frame.size.height);
    
    [background setPosition: CGPointMake(xLoc,0)];
    background.physicsBody.affectedByGravity = FALSE;
    background.physicsBody.categoryBitMask = backGroundCategory;
    [self addChild: (background)];
    

}
-(void) reset{
    [player removeFromParent];
    
    [fireBall removeFromParent];
    for(int i = 0; i < 6; i++){
        [imp[i] removeFromParent];
    }
    for(int i = 0; i < 2; i++){
        [mageImp[i] removeFromParent];
    }
    
    [self makePlayer];
    [self makeImps];
    [self makeMageImps];
    
    frameCounter = 0;
}

//-(void) makeGround:(int)leftLimit :(int)rightLimit {
-(void) makeGround{
    ground  = [SKSpriteNode spriteNodeWithImageNamed: @"ground.png"];
    ground.size = CGSizeMake(ground.texture.size.width * 3, ground.texture.size.height);

    [ground setPosition: CGPointMake(0,-518)];
    [ground setScale:2];
    
    ground.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(ground.size.width, 10)];
    ground.physicsBody.dynamic = NO;
    ground.physicsBody.affectedByGravity = FALSE;
    ground.physicsBody.categoryBitMask = groundCategory;
    [self addChild: (ground)];
    
    
    
}

-(void) makePlayer{
    //Set up player
    SKTexture* CelesGround = [SKTexture textureWithImageNamed:@"Celes.gif"];
    player = [SKSpriteNode spriteNodeWithTexture: CelesGround];
    celesIdle = CelesGround;
    playerLeft = false;
    [player setPosition: CGPointMake(0,-517)];
    //player.size = player.texture.size;
    player.size = CGSizeMake(80, 100);
    
    
    player.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:player.texture.size];
    player.physicsBody.dynamic = YES;
    player.physicsBody.allowsRotation = false;
    player.physicsBody.categoryBitMask = playerCategory;
    player.physicsBody.contactTestBitMask = impCategory | mageFireCategory;
    player.physicsBody.usesPreciseCollisionDetection = YES;
    
    playerHealth = 5;
    
    [self addChild: (player)];
}

-(void) makeImps{
    //Set up imo
    int impXpos[6];
    int sign;
    int randomNum;
    for(int i = 0; i < 6;i++){
        
        SKTexture* impStart;
        
        randomNum = arc4random_uniform(1500);
        
        sign = arc4random_uniform(2);
        while(randomNum > 0 -300 && randomNum < 0 + 300){
            randomNum = arc4random_uniform(1500);
            
        }
        if(sign == 1){
            impXpos[i] = randomNum * -1;
            impDirect[i] = false;
            impStart = [SKTexture textureWithImageNamed:@"impright1.tiff"];
            
        }
        else{
            impDirect[i] = true;
            impXpos[i] = randomNum;
            impStart = [SKTexture textureWithImageNamed:@"impleft1.tiff"];
           
        }
        //printf("x pos  %d \n",impXpos[i]);
        imp[i] = [SKSpriteNode spriteNodeWithTexture: impStart];
        //[imp[i] setTexture:impStart];
        
        [imp[i] setPosition: CGPointMake(impXpos[i],-517)];
        imp[i].size = CGSizeMake(80, 100);
        
        imp[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:imp[i].texture.size];
        imp[i].physicsBody.dynamic = YES;
        imp[i].physicsBody.allowsRotation = false;
        imp[i].physicsBody.categoryBitMask = impCategory;
        imp[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory | platformCategory;
        imp[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        imp[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"imp";
        NSString* impNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:impNum];
        
        imp[i].name = str;
        
        [self addChild: (imp[i])];
        
    }
}

-(void) makeMageImps{
    //Set up imo
    
    for(int i = 0; i < 2;i++){
        mageAlive[i] = true;
        SKTexture* impStart;
        int xPos;
        if(i == 0){
            mageDirect[i] = false;
            impStart = [SKTexture textureWithImageNamed:@"impright1.tiff"];
            xPos = -1260;

        }else{
            mageDirect[i] = true;
            impStart = [SKTexture textureWithImageNamed:@"impleft1.tiff"];
            xPos = 1260;
            
        }
        
        mageImp[i] = [SKSpriteNode spriteNodeWithTexture: impStart];
        //[imp[i] setTexture:impStart];
        [mageImp[i] setPosition: CGPointMake(xPos,-80)];

        
        mageImp[i].size = CGSizeMake(80, 100);
        
        mageImp[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:imp[i].texture.size];
        mageImp[i].physicsBody.dynamic = YES;
        mageImp[i].physicsBody.allowsRotation = false;
        mageImp[i].physicsBody.categoryBitMask = impCategory;
        mageImp[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory |platformCategory;
        mageImp[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        mageImp[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"mageimp";
        NSString* impNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:impNum];
        
        mageImp[i].name = str;
        
        [self addChild: (mageImp[i])];
        
    }
}

-(void) makeBirds{
    //Set up imo
    
    for(int i = 0; i < 2;i++){
        
        SKTexture* birdStart;
        int xPos;
        if(i == 0){
            birdStart = [SKTexture textureWithImageNamed:@"bird1.tiff"];
            birdDirect[0] = true;
            xPos = -1000;
            //moveLeftSide = [SKAction moveToX:xPos duration:8];
            moveLeftSide = [SKAction moveTo: CGPointMake(xPos, 250) duration: 6];
            
        }else{
            birdStart = [SKTexture textureWithImageNamed:@"bird2.tiff"];
            xPos = 1000;
            birdDirect[1] = false;
            //moveRightSide = [SKAction moveToX:xPos y:250 duration:8];
            moveRightSide = [SKAction moveTo: CGPointMake(xPos, 250) duration: 6];
            
        }
        
        bird[i] = [SKSpriteNode spriteNodeWithTexture: birdStart];
        [bird[i] setPosition: CGPointMake(xPos,250)];
        
        
        bird[i].size = CGSizeMake(100, 120);
        
        bird[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:bird[i].texture.size];
        bird[i].physicsBody.dynamic = YES;
        bird[i].physicsBody.affectedByGravity = FALSE;
        bird[i].physicsBody.allowsRotation = false;
        bird[i].physicsBody.categoryBitMask = impCategory;
        bird[i].physicsBody.collisionBitMask = groundCategory | fireBallCategory | playerCategory;
        bird[i].physicsBody.contactTestBitMask = playerCategory | fireBallCategory;
        bird[i].physicsBody.usesPreciseCollisionDetection = YES;
        
        //printf("Times im running \n");
        
        NSString *str = @"bird";
        NSString* birdNum = [NSString stringWithFormat:@"%i", i];
        str = [str stringByAppendingString:birdNum];
        
        bird[i].name = str;
        
        [self addChild: (bird[i])];
        
        [bird[i] runAction:birdFly];

        
    }
}



-(void) makePlatform {
    for(int i = 0; i < 11; i++){
        platform[i] = [SKSpriteNode spriteNodeWithImageNamed:@"Bar.png"];
        platform[i].size = CGSizeMake(160, 40);
        
        platform[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(platform[i].size.width, platform[i].size.height)];
        platform[i].physicsBody.dynamic = NO;
        platform[i].physicsBody.affectedByGravity = FALSE;
        platform[i].physicsBody.categoryBitMask = platformCategory;
        platform[i].physicsBody.contactTestBitMask = playerCategory | impCategory;
        platform[i].physicsBody.usesPreciseCollisionDetection = YES;
    }
    [platform[0] setPosition:CGPointMake(-400, -350)];
    [platform[1] setPosition:CGPointMake(400, -350)];
    [platform[2] setPosition:CGPointMake(1000, -350)];
    [platform[3] setPosition:CGPointMake(-1000, -350)];
    [platform[4] setPosition:CGPointMake(700, -150)];
    [platform[5] setPosition:CGPointMake(-700, -150)];
    [platform[6] setPosition:CGPointMake(1300, -150)];
    [platform[7] setPosition:CGPointMake(-1300, -150)];
    [platform[8] setPosition:CGPointMake(-400, 0)];
    [platform[9] setPosition:CGPointMake(400, 0)];
    [platform[10] setPosition:CGPointMake(0, 100)];

    
    for(int i = 0; i < 11; i++){
        [self addChild: (platform[i])];
    }
}


-(void) makeAnimation{
    
    //Walk action Left
    SKTexture* CelesWalkLeft1 = [SKTexture textureWithImageNamed:@"walkLeft1.tiff"];
    CelesWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft2 = [SKTexture textureWithImageNamed:@"walkLeft2.tiff"];
    CelesWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft3 = [SKTexture textureWithImageNamed:@"walkLeft3.tiff"];
    CelesWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkLeft4 = [SKTexture textureWithImageNamed:@"walkLeft4.tiff"];
    CelesWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    walkLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkLeft1,CelesWalkLeft2,CelesWalkLeft3,CelesWalkLeft4] timePerFrame:0.2]];
    
    celesLeft = CelesWalkLeft1;

    
    //Walk action Right
    SKTexture* CelesWalkRight1 = [SKTexture textureWithImageNamed:@"walkRight1.tiff"];
    CelesWalkRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight2 = [SKTexture textureWithImageNamed:@"walkRight2.tiff"];
    CelesWalkRight2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight3 = [SKTexture textureWithImageNamed:@"walkRight3.tiff"];
    CelesWalkRight3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesWalkRight4 = [SKTexture textureWithImageNamed:@"walkRight4.tiff"];
    CelesWalkRight4.filteringMode = SKTextureFilteringNearest;
    
    
    walkRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesWalkRight1,CelesWalkRight2,CelesWalkRight3,CelesWalkRight4] timePerFrame:0.2]];
    
    celesRight = CelesWalkRight1;

    
    //Jump action Left
    SKTexture* CelesJumpLeft1 = [SKTexture textureWithImageNamed:@"jumpLeft1.png"];
    CelesJumpLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesJumpLeft2 = [SKTexture textureWithImageNamed:@"jumpLeft2.tiff"];
    CelesJumpLeft2.filteringMode = SKTextureFilteringNearest;
    
    jumpLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpLeft1, CelesJumpLeft2] timePerFrame:0.2]];
    
    //Jump action Right
    SKTexture* CelesJumpRight1 = [SKTexture textureWithImageNamed:@"jumpRight1.tiff"];
    CelesJumpRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesJumpRight2 = [SKTexture textureWithImageNamed:@"jumpRight2.tiff"];
    CelesJumpRight2.filteringMode = SKTextureFilteringNearest;
    
    jumpRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[CelesJumpRight1, CelesJumpRight2] timePerFrame:0.2]];
    
    //Death
    celesHit = [SKTexture textureWithImageNamed:@"CelesHit.gif"];
    celesHit.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesDeath1 = [SKTexture textureWithImageNamed:@"CelesDeath1.gif"];
    CelesDeath1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* CelesDeath2 = [SKTexture textureWithImageNamed:@"CelesDeath2.gif"];
    CelesDeath2.filteringMode = SKTextureFilteringNearest;
    
    death = [SKAction animateWithTextures:@[celesHit, CelesDeath1,CelesDeath2] timePerFrame:0.5];
    
    hit = [SKAction animateWithTextures:@[celesHit] timePerFrame:0.2];
    
    //Bad guy animation
    
    
    //Walk action Left
    SKTexture* ImpWalkLeft1 = [SKTexture textureWithImageNamed:@"impleft1.tiff"];
    ImpWalkLeft1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkLeft2 = [SKTexture textureWithImageNamed:@"impleft2.tiff"];
    ImpWalkLeft2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkLeft3 = [SKTexture textureWithImageNamed:@"impleft3.tiff"];
    ImpWalkLeft3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkLeft4 = [SKTexture textureWithImageNamed:@"impleft4.tiff"];
    ImpWalkLeft4.filteringMode = SKTextureFilteringNearest;
    
    
    impLeft = [SKAction repeatActionForever:[SKAction animateWithTextures:@[ImpWalkLeft1,ImpWalkLeft2,ImpWalkLeft3,ImpWalkLeft4] timePerFrame:0.2]];
    
    
    //Walk action Right
    SKTexture* ImpWalkRight1 = [SKTexture textureWithImageNamed:@"impright1.tiff"];
    ImpWalkRight1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkRight2 = [SKTexture textureWithImageNamed:@"impright2.tiff"];
    ImpWalkRight2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkRight3 = [SKTexture textureWithImageNamed:@"impright3.tiff"];
    ImpWalkRight3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* ImpWalkRight4 = [SKTexture textureWithImageNamed:@"impright4.tiff"];
    ImpWalkRight4.filteringMode = SKTextureFilteringNearest;
    
    
    impRight = [SKAction repeatActionForever:[SKAction animateWithTextures:@[ImpWalkRight1,ImpWalkRight2,ImpWalkRight3,ImpWalkRight4] timePerFrame:0.2]];
    
    //Bird Fly
    SKTexture* fly1 = [SKTexture textureWithImageNamed:@"bird1.tiff"];
    fly1.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* fly2 = [SKTexture textureWithImageNamed:@"bird2.tiff"];
    fly2.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* fly3 = [SKTexture textureWithImageNamed:@"bird3.tiff"];
    fly3.filteringMode = SKTextureFilteringNearest;
    
    SKTexture* fly4 = [SKTexture textureWithImageNamed:@"bird4.tiff"];
    fly4.filteringMode = SKTextureFilteringNearest;
    
    birdFly = [SKAction repeatActionForever:[SKAction animateWithTextures:@[fly1,fly2,fly3,fly4] timePerFrame:0.2]];
    
    birdDead = [SKTexture textureWithImageNamed:@"deadBird.gif"];
    birdDead.filteringMode = SKTextureFilteringNearest;

    
}
-(void)makeFireBall{
    [fireBall removeFromParent];
    fireBall = [SKSpriteNode spriteNodeWithImageNamed: @"Firework3.gif"];
    //Fireball to the left or right of the player
    if(playerLeft == false){
        [fireBall setPosition: CGPointMake(player.position.x + 30,player.position.y +10)];
    }
    else{
         [fireBall setPosition: CGPointMake(player.position.x - 30,player.position.y + 10)];
    }
    fireBall.size = CGSizeMake(60, 60);
    
    fireBall.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:fireBall.texture.size];
    
    fireBall.physicsBody.affectedByGravity = false;
    fireBall.physicsBody.allowsRotation = false;
    fireBall.physicsBody.categoryBitMask = fireBallCategory;
    fireBall.physicsBody.contactTestBitMask = impCategory;
    fireBall.physicsBody.usesPreciseCollisionDetection = YES;
    [self addChild: fireBall];
}

-(void)makeMageFireBall{
    for(int i = 0; i < 2; i++){
        [mageFireBall[i] removeFromParent];
        if(mageAlive[i] == true){
            mageFireBall[i] = [SKSpriteNode spriteNodeWithImageNamed: @"Firework3.gif"];
            //Fireball to the left or right of the player
            if(mageDirect == false){
                [mageFireBall[i] setPosition: CGPointMake(mageImp[i].position.x + 40,mageImp[i].position.y +10)];
            }
            else{
                [mageFireBall[i] setPosition: CGPointMake(mageImp[i].position.x - 40,mageImp[i].position.y + 10)];
            }
            mageFireBall[i].size = CGSizeMake(60, 60);
    
            mageFireBall[i].physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:mageFireBall[i].texture.size];
    
            mageFireBall[i].physicsBody.affectedByGravity = false;
            mageFireBall[i].physicsBody.allowsRotation = false;
            mageFireBall[i].physicsBody.categoryBitMask = mageFireCategory;
            mageFireBall[i].physicsBody.contactTestBitMask = playerCategory|platformCategory|groundCategory;
            mageFireBall[i].physicsBody.usesPreciseCollisionDetection = YES;
        
            NSString *str = @"mageFire";
            NSString* fireNum = [NSString stringWithFormat:@"%i", i];
            str = [str stringByAppendingString:fireNum];
        
            mageFireBall[i].name = str;
        
            [self addChild: mageFireBall[i]];
        }
    }
}

-(void) mageAttack{
    [self makeMageFireBall];
    int playerX = player.position.x;
    int playerY = player.position.y;
    SKAction *goToPlayer;
    for(int i = 0; i < 2; i++){
        if(mageAlive[i] == true){
            goToPlayer = [SKAction moveTo: CGPointMake(playerX, playerY) duration: 6];
            [mageFireBall[i] runAction:goToPlayer];
        }
    }
}

//fire button
- (void)makeFireButton
{
    fireButton = [SKSpriteNode spriteNodeWithImageNamed:@"button.png"];
    fireButton.size = CGSizeMake(100 ,100);
    fireButton.position = CGPointMake(player.position.x,-620);
    fireButton.name = @"fireButtonNode";//how the node is identified later

    [self addChild: (fireButton)];
    
}

-(void) makeResetButton {
    resetButton = [SKSpriteNode spriteNodeWithImageNamed:@"reset.png"];
    resetButton.size = CGSizeMake(100 ,100);
    resetButton.position = CGPointMake(player.position.x + 270,-620);
    resetButton.name = @"reset";//how the node is identified later
    
    [self addChild: (resetButton)];
}


-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    SKNode *node = [self nodeAtPoint:location];
    //printf("Player loc %f vs touch location %f \n",player.position.x, location.x);
    if ([node.name isEqualToString:@"fireButtonNode"] && playerHealth >0) {
        [self makeFireBall];
        if(playerLeft == true){
            [fireBall.physicsBody applyImpulse:CGVectorMake(-15, 0)];
        }
        else{
            [fireBall.physicsBody applyImpulse:CGVectorMake(15, 0)];
        }
    }
    else if ([node.name isEqualToString:@"reset"]) {
        [self reset];
    }
    else{
        if(playerHealth > 0){
            if(location.y <= player.position.y){
                //if(player.physicsBody.velocity.dx == 0){
                if(location.x > player.position.x && location.y){
                    //[background[0] runAction: moveBackground];
                    playerLeft = false;
                    [player.physicsBody applyImpulse:CGVectorMake(4, 0)];
            
                    [player runAction:walkRight];
            
                }
                else{
                    playerLeft = true;
                    [player.physicsBody applyImpulse:CGVectorMake(-4, 0)];
                    [player runAction:walkLeft];
            
                }
            }
            else{
                if(player.physicsBody.velocity.dy == 0){
                    if(location.x > player.position.x && location.y){
                        playerLeft = false;
                        [player.physicsBody applyImpulse:CGVectorMake(5, 30)];
                        [player runAction:jumpRight];
                
                    }
                    else{
                        playerLeft = true;
                        [player.physicsBody applyImpulse:CGVectorMake(-5, 30)];
                        [player runAction:jumpLeft];
                
                    }
                }
            }
        
        }
    }
    
    
}

-(void) didBeginContact:(SKPhysicsContact *)contact
{
    
    SKPhysicsBody *firstBody, *secondBody;
    if (contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask)
    {
        firstBody = contact.bodyA;
        secondBody = contact.bodyB;
    }
    else
    {
        firstBody = contact.bodyB;
        secondBody = contact.bodyA;
    }
    if ((firstBody.categoryBitMask == impCategory)&&
     (secondBody.categoryBitMask == impCategory))
     {
         //Figure out how to unclog imps
     }
    
     if ((firstBody.categoryBitMask == impCategory)&& (secondBody.categoryBitMask == fireBallCategory))
     {
         printf("Fireball hit");
         NSLog(@"%@ \n" , firstBody.node.name);
         //How know what object to remove
         SKTexture* DeadImp = [SKTexture textureWithImageNamed:@"ImpHit.gif"];
         if([firstBody.node.name  isEqualToString: @"imp0"]){
             [imp[0] removeAllActions];
             [imp[0] setTexture:DeadImp];
             [imp[0] removeFromParent];
             impScore++;
         }
         if([firstBody.node.name  isEqualToString: @"imp1"]){
             [imp[1] removeAllActions];
             [imp[1] setTexture:DeadImp];
             [imp[1] removeFromParent];
             impScore++;

         }
         if([firstBody.node.name  isEqualToString: @"imp2"]){
             [imp[2] removeAllActions];
             [imp[2] setTexture:DeadImp];
             [imp[2] removeFromParent];
             impScore++;

         }
         if([firstBody.node.name  isEqualToString: @"imp3"]){
             [imp[3] removeAllActions];
             [imp[3] setTexture:DeadImp];
             [imp[3] removeFromParent];
             impScore++;

         }
         if([firstBody.node.name  isEqualToString: @"imp4"]){
             [imp[4] removeAllActions];
             [imp[4] setTexture:DeadImp];
             [imp[4] removeFromParent];
             impScore++;

         }
         if([firstBody.node.name  isEqualToString: @"imp5"]){
             [imp[5] removeAllActions];
             [imp[5] setTexture:DeadImp];
             [imp[5] removeFromParent];
             impScore++;

         }
         if([firstBody.node.name  isEqualToString: @"mageimp0"]){
             mageAlive[0] = false;
             [mageImp[0] removeAllActions];
             [mageImp[0] setTexture:DeadImp];
             [mageImp[0] removeFromParent];
             impScore++;

         }
         if([firstBody.node.name  isEqualToString: @"mageimp1"]){
             mageAlive[1] = false;
             [mageImp[1] removeAllActions];
             [mageImp[1] setTexture:DeadImp];
             [mageImp[1] removeFromParent];
             impScore++;

         }
         
         if([firstBody.node.name  isEqualToString: @"bird0"]){
             [bird[0] removeAllActions];
             [bird[0] setTexture:birdDead];
             [bird[0] removeFromParent];
             impScore++;
             
         }
         if([firstBody.node.name  isEqualToString: @"bird1"]){
             [bird[1] removeAllActions];
             [bird[1] setTexture:birdDead];
             [bird[1] removeFromParent];
             impScore++;
             
         }
         //Change to dead imp
         [fireBall removeFromParent];

     
     }
    
    if ((firstBody.categoryBitMask == impCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        //WHY IS THIS BREAKING NOT SURE
        //printf("%@ %@ \n", firstBody, secondBody);
        //NSLog(@" %@ %@ \n", firstBody, secondBody);
        printf("Collison");
        playerHealth --;
        if(playerHealth == 0){
            //gameOver
            NSLog(@"Player died");
            [player removeAllActions];
            [player runAction:death];
        }
        else{
            [player removeAllActions];
            [player runAction: hit];
        }
        
    }
    if ((firstBody.categoryBitMask == mageFireCategory)&&
        (secondBody.categoryBitMask == playerCategory))
    {
        playerHealth --;
        if(playerHealth == 0){
            //gameOver
            [player removeAllActions];
            [player runAction:death];
        }
        else if(playerHealth > 0){
            [player removeAllActions];
            
            //[player setTexture:celesHit];
            [player runAction: hit];
            
        }
        
        
        if([firstBody.node.name  isEqualToString: @"mageFire0"]){
            [mageFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"mageFire1"]){
            [mageFireBall[1] removeFromParent];
        }

        
        
    }
    if ((firstBody.categoryBitMask == mageFireCategory)&&
        (secondBody.categoryBitMask == platformCategory))
    {
        //printf("%@ %@ \n", firstBody, secondBody);
        
        if([firstBody.node.name  isEqualToString: @"mageFire0"]){
            [mageFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"mageFire1"]){
            [mageFireBall[1] removeFromParent];
        }
    }
    
    if ((firstBody.categoryBitMask == mageFireCategory)&&
        (secondBody.categoryBitMask == groundCategory))
    {
        //printf("%@ %@ \n", firstBody, secondBody);
        
        if([firstBody.node.name  isEqualToString: @"mageFire0"]){
            [mageFireBall[0] removeFromParent];
        }
        if([firstBody.node.name  isEqualToString: @"mageFire1"]){
            [mageFireBall[1] removeFromParent];
        }
    }

    
}


-(void)update:(CFTimeInterval)currentTime {
    // Called before each frame is rendered
    //cam.position = player.position;
    cam.position = CGPointMake(player.position.x, CGRectGetMidY(self.frame));
    
    //Keep button on player
    fireButton.position = CGPointMake(player.position.x, -620);
    
    resetButton.position = CGPointMake(player.position.x + 270, -620);
    
    //Imps movement
    int yJump;
    int xSpeed;
    for(int i = 0; i < 6; i++){
        yJump = arc4random_uniform(35);
        xSpeed = arc4random_uniform(15);
        if(imp[i].physicsBody.velocity.dx == 0){
            if(impDirect[i] == true){
                [imp[i].physicsBody applyImpulse:CGVectorMake(-1 * xSpeed, yJump)];
                [imp[i] runAction:impLeft];
            }
            else{
                [imp[i].physicsBody applyImpulse:CGVectorMake(xSpeed, yJump)];
                [imp[i] runAction:impRight];
            }
        }
        //Keeps imp in bounds
        if(imp[i].position.x > 1400){
            impDirect[i] = true;
        }
        else if(imp[i].position.x < -1400){
            impDirect[i] = false;
        }
    }
    for(int i = 0; i < 2; i++){
        if(mageImp[i].physicsBody.velocity.dy == 0){
            [mageImp[i].physicsBody applyImpulse:CGVectorMake(0, 30)];
        }
    }
    
    for(int i = 0; i < 2; i++){
        if(bird[i].physicsBody.velocity.dx == 0){
            if(birdDirect[i] == true){
                [bird[i] runAction:moveRightSide];
            }
            else{
                [bird[i] runAction:moveLeftSide];
            }
        }
        if(bird[i].position.x > 900){
            birdDirect[i] = false;
        }
        else if(bird[i].position.x < -900){
            birdDirect[i] = true;
        }
    }
    
    
    // Initialize _lastUpdateTime if it has not already been
    if (_lastUpdateTime == 0) {
        _lastUpdateTime = currentTime;
    }
    
    // Calculate time since last update
    CGFloat dt = currentTime - _lastUpdateTime;
    
    // Update entities
    for (GKEntity *entity in self.entities) {
        [entity updateWithDeltaTime:dt];
    }
    if(player.physicsBody.velocity.dx == 0 && player.physicsBody.velocity.dy == 0 && playerHealth > 0){
        [player removeAllActions];
        if(player.position.x < 20 && player.position.x > -20){
            [player setTexture: celesIdle];
        }
        else{
            if(playerLeft == true){
                [player setTexture: celesLeft];
            }else{
                [player setTexture:celesRight];
            }
        }
    }
    
    if (frameCounter == 30*4){
        [self mageAttack];
        frameCounter = 0;
    }
    
    //Below screen
    if(player.position.y < -650){
        playerHealth = 0;
        [player removeFromParent];
    }
    
    _lastUpdateTime = currentTime;
    frameCounter++;
}

@end
